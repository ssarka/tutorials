
---
title: "GraphQL Fundamentals"
author: Saikat Sarkar @ssarka
date: 2020-12-03
styles:
    table:
        column_spacing: 5
---

# Introduction

| Saikat Sarkar  | Secure, Static Analysis | GitLab |
|----------------|-------------------------|--------|


```
                                      :..                          .-..        
                                    .:..:.                        .:.-:        
                                    :.  :..                       :.  :.       
                                   :. .  :.                      ::    :.      
                                  .: .    :.                    :-     .:.     
                                 .:.      .:.                  .:       .:     
                                 :.        .:                 .:.        ::    
                                :---.:.:.-.:.::..-..-..-..-..:.:..:..-.:.:.-   
                               -:.-..        :..   .     .  .--        ..:.:.  
                              .:.  -:.       .:.            --        :..  .:. 
                              :.     .:.       :.          .: .     .:.     .- 
                             -:.       :..     :-          :.     .:.       :.:
                               .:...     :. .   ::        ::     :-     ...:.  
                                  .... .  .:- .  :.      :-   .::     :.-.     
                                    . :...  .:. . :.    .: . :.  . ::. .       
                                        ..:....:...:   .:...:...:.-            
                                           ...-.:.:.: .:.:.:.-..               
                                               ..:.:::-:.:...                  
                                                   :..:.. .
```

# Topics to discuss
## REST API (Pure)
## REST API vs GraphQL
## GraphQL operations
## Round-trips
## REST-ISH API vs Pure REST API vs GraphQL
## What is GraphQL exactly?
## Misconceptions
## Resources
## Questions

# REST API (Pure)
## REST = Representational state transfer
## It's all about a resource which can be a singleton or a collection.
## Examples
  * GET <host address>/v4/books
  * GET <host address>/v4/books/234
  * POST <host address>/v4/books 
  * PUT <host address>/v4/books/234
  * DELETE <host address>/v4/books/234 

# REST API vs GraphQL
## A number of REST api endpoints to manage a resouce
  * GET, POST, PUT, PATCH, DELETE

## A single GraphQL api endpoint for the whole application
  * POST <host address>/graphql 

## No need to define new route on the server-side.  

# REST API vs GraphQL
## No more under-fetching and over-fetching of data
### REST API
  * Request: GET <host address>/v4/books/234
  * Response: 
            { id: 234, 
                title: 'GraphQL in Action', 
                author_id: 34, 
                price: '$234', 
                publication_date: "2017-03-20" }

### GraphQL
  * Request: POST <host address>/graphql --data '{ "query": "{ book(id:234) { title } }" }'
  * Response: { title: 'GraphQL in Action'}

# REST API vs GraphQL
## Versioning
### REST API
  - Resquest: GET <host address>/v3/books/123
     Response: {id: 234, title: "GrapQL in Action", cost: "$45"} 

  - Resquest: GET <host address>/v4/books/123
     Response: {id: 234, title: "GrapQL in Action", price: "$45"} 

### GraphQL
  1. Request: POST <host address>/graphql --data '{ "query": "{ book(id:234) { title \n cost } }" }'
     Response: { title: 'GraphQL in Action', cost: "$45"}
  2. Request: POST <host address>/graphql --data '{ "query": "{ book(id:234) { title \n price } }" }'
     Response: { title: 'GraphQL in Action', price: "$45"}

# REST API vs GraphQL
## Documentation
### GraphQL documentation is built-in
### In order to create GraphQL API, you need to provide a typed schema. GraphQL forces to write proper documentation for the fields defined in the schema.
### Eventually GraphQL makes it mandatory for the server to publish the documentation.


# REST API vs GraphQL
## Insightful Analytics on the Backend
### GraphQL allows you to have fine-grained insights about the data that’s requested on the backend.
### Low-level performance monitoring of the requests that are processed by your server.
### For example, in the following resource, GraphQL can facilitate to provide telemetry at more granular level (title, price or publicaton_date)
  * Book: 
            { id: 234, 
                title: 'GraphQL in Action', 
                author_id: 34, 
                price: '$234', 
                publication_date: "2017-03-20" }

# GraphQL operations
## Query
 * Read data.
 * Similar to GET in REST api.

## Mutation
 * Modify data.
 * Similar to POST, PUT, DELETE, PATCH in REST api.

## Subscription
 * continuous pushing of data to clients
 * websocket used in Facebook messager or any chat applications.

# Round-trips
## Multiple requests for REST api

```
                +---------+                                                      +---------+
                |         |     1. Data about books please                       |         |
                |         |----------------------------------------------------->|         |
                |         |     2. Data about authors please                     |         |
                |  Client |----------------------------------------------------->| Server  |
                |         |     3. Data about reviews please                     |         |
                |         |----------------------------------------------------->|         |
                |         |                                                      |         |
                +---------+                                                      +---------+
```

## Single request for GraphQL api

```
                +---------+                                                      +---------+
                |         |     1. Data about books, authors, reviews please     |         |
                |         |----------------------------------------------------->|         |
                |         |                                                      |         |
                |  Client |                                                      | Server  |
                |         |                                                      |         |
                |         |                                                      |         |
                |         |                                                      |         |
                +---------+                                                      +---------+
```

# REST-ISH api vs Pure REST api vs GraphQL
## We can modify REST api to eliminate some of the problems that GraphQL tries to solve here. These customized APIs are called REST-ISH apis.
## For example, we can pass a query string containing a list of required fields.
## REST-ISH api is ok for small projects. But, this requires a lot of work for large projects.
## REST-ISH api is not standardized. 

# What is GraphQL exactly?
## GraphQL solves communication problems suffered by REST api.
## It is a specification
  - There are a number of implementations of GraphQL.
  - Current specification document: https://spec.graphql.org/June2018/

## It is a declarative language
  -  GraphQL can be used for both reading and modifying data which is similar to SQL.

# Misconceptions
## GraphQL can only be used by React. (No)
 - It can be implemented in any language and it can be used with any web framework.
 - GraphQL is just a standard.

## GraphQL is related to Graph database. (No)
 - Any datastore can be used as backend. For example, RDBMS, NoSQL, Graph database, and even a simple JSON file.

## GraphQL is all about storing the data in a graph data structure and traversing this graph to retrive data. (No)
 - The word 'Graph' in GraphQL comes from the idea that best way to represent data in the real world is with a graph-like data structure. It's all about how you view data not how you store data.

## GraphQL solves N+1 problem of database query. (No)
 - GraphQL tries to address the problem of N+1 request to API Server. But, it is not reponsible for looking into how the API server communicates with the backend database.

# Resources
### Samer Buna, [GraphQL in Action](https://www.manning.com/books/graphql-in-action)
### GraphQL Official page: https://graphql.org/
### GitLab's documentation on GraphQL: https://docs.gitlab.com/ee/api/graphql/

# Questions

```
                                      :..                          .-..        
                                    .:..:.                        .:.-:        
                                    :.  :..                       :.  :.       
                                   :. .  :.                      ::    :.      
                                  .: .    :.                    :-     .:.     
                                 .:.      .:.                  .:       .:     
                                 :.        .:                 .:.        ::    
                                :---.:.:.-.:.::..-..-..-..-..:.:..:..-.:.:.-   
                               -:.-..        :..   .     .  .--        ..:.:.  
                              .:.  -:.       .:.            --        :..  .:. 
                              :.     .:.       :.          .: .     .:.     .- 
                             -:.       :..     :-          :.     .:.       :.:
                               .:...     :. .   ::        ::     :-     ...:.  
                                  .... .  .:- .  :.      :-   .::     :.-.     
                                    . :...  .:. . :.    .: . :.  . ::. .       
                                        ..:....:...:   .:...:...:.-            
                                           ...-.:.:.: .:.:.:.-..               
                                               ..:.:::-:.:...                  
                                                   :..:.. .
```

